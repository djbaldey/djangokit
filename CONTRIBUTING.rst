=================
How to contribute
=================

As an open source project, DjangoKit welcomes contributions of many forms.

Examples of contributions include:

* Code patches
* Translations
* Documentation improvements
* Bug reports and patch reviews


Getting Started
---------------

TODO: continue this guide
