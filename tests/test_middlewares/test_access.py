#
# Copyright (c) 2022, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.test import SimpleTestCase, TestCase, modify_settings, \
    override_settings


class MaintenanceMiddlewareTestCase(SimpleTestCase):

    MODIFY_MIDDLEWARE = {
        'append': 'djangokit.middleware.access.MaintenanceMiddleware'}

    @override_settings(MAINTENANCE=True)
    @modify_settings(MIDDLEWARE=MODIFY_MIDDLEWARE)
    def test_on(self):
        with self.assertTemplateUsed('maintenance.html'):
            response = self.client.get('/ok')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.get('content-type'),
                         'text/html; charset=utf-8')

    @override_settings(MAINTENANCE=False)
    @modify_settings(MIDDLEWARE=MODIFY_MIDDLEWARE)
    def test_off(self):
        with self.assertTemplateNotUsed('maintenance.html'):
            response = self.client.get('/ok')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('content-type'),
                         'text/plain; charset=utf-8')
        self.assertEqual(response.content, b'OK')


class SeparateAccessMiddlewareTestCase(TestCase):

    MODIFY_MIDDLEWARE = {
        'append': 'djangokit.middleware.access.SeparateAccessMiddleware'}

    @override_settings(SEPARATE_ACCESS_KEY='1')
    @modify_settings(MIDDLEWARE=MODIFY_MIDDLEWARE)
    def test_on(self):
        with self.assertTemplateUsed('separate_access.html'):
            response = self.client.get('/ok')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.get('content-type'),
                         'text/html; charset=utf-8')

        self.client.post('/ok', data={'access_key': '1'})

        with self.assertTemplateNotUsed('separate_access.html'):
            response = self.client.get('/ok')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('content-type'),
                         'text/plain; charset=utf-8')
        self.assertEqual(response.content, b'OK')

    @override_settings(SEPARATE_ACCESS_KEY='')
    @modify_settings(MIDDLEWARE=MODIFY_MIDDLEWARE)
    def test_off(self):
        with self.assertTemplateNotUsed('separate_access.html'):
            response = self.client.get('/ok')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('content-type'),
                         'text/plain; charset=utf-8')
        self.assertEqual(response.content, b'OK')
