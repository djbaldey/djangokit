#
# Copyright (c) 2022, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.test import SimpleTestCase, modify_settings, override_settings


MODIFY_MIDDLEWARE = {'append': 'djangokit.middleware.profiler.profile'}


class ProfilerTestCase(SimpleTestCase):

    @override_settings(DEBUG=True)
    @modify_settings(MIDDLEWARE=MODIFY_MIDDLEWARE)
    def test_profile_get(self):
        # Normal response.
        response = self.client.get('/ok')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('content-type'),
                         'text/plain; charset=utf-8')
        self.assertEqual(response.content, b'OK')

        # By param.
        response = self.client.get('/ok?__profile_view')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('content-type'),
                         'text/plain; charset=utf-8')
        self.assertIn(b'function calls', response.content)

        # By header.
        response = self.client.get('/ok', HTTP_PROFILE_VIEW='true')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('content-type'),
                         'text/plain; charset=utf-8')
        self.assertIn(b'function calls', response.content)

    @override_settings(DEBUG=False)
    @modify_settings(MIDDLEWARE=MODIFY_MIDDLEWARE)
    def test_profile_debug_only(self):
        # By param.
        response = self.client.get('/ok?__profile_view')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('content-type'),
                         'text/plain; charset=utf-8')
        self.assertEqual(response.content, b'OK')

        # By header.
        response = self.client.get('/ok', HTTP_PROFILE_VIEW='true')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('content-type'),
                         'text/plain; charset=utf-8')
        self.assertEqual(response.content, b'OK')
