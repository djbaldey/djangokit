#
# Copyright (c) 2022, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.conf import settings
from django.test import TestCase, override_settings


class DomainSessionMiddleware(TestCase):

    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'djangokit.middleware.subdomains.DomainSessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]

    @override_settings(MIDDLEWARE=MIDDLEWARE)
    def test_on(self):
        response = self.client.get('/visited')
        self.assertEqual(response.status_code, 200)
        self.assertIn(settings.SESSION_COOKIE_NAME, response.cookies)
