#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.core.exceptions import ImproperlyConfigured
from django.test import SimpleTestCase
from djangokit.utils.apps import DependencesMixin, get_dependences_map


class AppsTestCase(SimpleTestCase):

    def test_mixin(self):
        mixin = DependencesMixin()
        self.assertTrue(hasattr(mixin, 'dependences'))
        self.assertTrue(hasattr(mixin, 'check_dependences'))
        self.assertTrue(hasattr(mixin, 'ready'))
        mixin.dependences = [
            'django.contrib.admin',
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.messages',
            'django.contrib.staticfiles',
            'djangokit',
        ]
        self.assertIsNone(mixin.check_dependences())
        self.assertIsNone(mixin.ready())
        # Для проверки поднятия ошибки нужно чтобы у экземпляра было имя.
        # При нормальном определении в коде, оно устанавливается из
        # родительского класса django.apps.AppConfig.
        mixin.name = 'test_mixin'
        mixin.dependences.append('abra')
        with self.assertRaises(ImproperlyConfigured):
            mixin.check_dependences()
        with self.assertRaises(ImproperlyConfigured):
            mixin.ready()

    def test_get_dependences_map(self):
        L = get_dependences_map()
        self.assertGreater(len(L), 6)
        self.assertEqual(L[0][0], 'django.contrib.admin')
        self.assertEqual(L[1][0], 'django.contrib.auth')
        self.assertEqual(L[2][0], 'django.contrib.contenttypes')
        self.assertEqual(L[3][0], 'django.contrib.sessions')
        self.assertEqual(L[4][0], 'django.contrib.messages')
        self.assertEqual(L[5][0], 'django.contrib.staticfiles')
        self.assertEqual(L[6][0], 'djangokit')

        L = get_dependences_map('django')
        self.assertGreater(len(L), 5)
        self.assertEqual(L[0][0], 'django.contrib.admin')
        self.assertEqual(L[1][0], 'django.contrib.auth')
        self.assertEqual(L[2][0], 'django.contrib.contenttypes')
        self.assertEqual(L[3][0], 'django.contrib.sessions')
        self.assertEqual(L[4][0], 'django.contrib.messages')
        self.assertEqual(L[5][0], 'django.contrib.staticfiles')

        L = get_dependences_map('djangokit')
        self.assertGreater(len(L), 0)
        self.assertEqual(L[0][0], 'djangokit')
