#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.test import SimpleTestCase
from djangokit.utils.crypto import AESCipher


class CryptoTestCase(SimpleTestCase):

    def test_aescipher(self):
        tests = (
            ('', ''),
            ('', '1'),
            ('', '1234567890'),
            ('', '1234567890123456'),
            ('', 'Мама мыла раму'),
            ('Пароль на русском языке и очень длинный', 'Мама мыла раму.'),
            ('', 'Мама мыла раму и ругала сына за то, что сидит в компе.'),
        )
        for password, message in tests:
            # AES-128-CBC
            cipher = AESCipher(password)
            self.assertEqual(str(cipher), 'AES-128-CBC')
            self.assertEqual(cipher.name, 'AES-128-CBC')
            self.assertEqual(len(cipher.password), 16)
            encrypted = cipher.encrypt(message)
            decrypted = cipher.decrypt(encrypted)
            self.assertEqual(decrypted, message)
            # AES-192-CBC
            cipher = AESCipher(password, size=24)
            self.assertEqual(str(cipher), 'AES-192-CBC')
            self.assertEqual(cipher.name, 'AES-192-CBC')
            self.assertEqual(len(cipher.password), 24)
            encrypted = cipher.encrypt(message)
            decrypted = cipher.decrypt(encrypted)
            self.assertEqual(decrypted, message)
            # AES-256-CBC
            cipher = AESCipher(password, size=32)
            self.assertEqual(str(cipher), 'AES-256-CBC')
            self.assertEqual(cipher.name, 'AES-256-CBC')
            self.assertEqual(len(cipher.password), 32)
            encrypted = cipher.encrypt(message)
            decrypted = cipher.decrypt(encrypted)
            self.assertEqual(decrypted, message)
