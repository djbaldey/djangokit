#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
import datetime
from django.test import SimpleTestCase, override_settings
from django.utils import timezone
from djangokit.utils import datetimes


class DatetimesTestCase(SimpleTestCase):

    def test_from_unixtime(self):
        tz = timezone.get_current_timezone()
        dt = datetimes.from_unixtime(0)
        dt = datetimes.from_unixtime('0')
        self.assertEqual(dt.year, 1970)
        self.assertEqual(dt.month, 1)
        self.assertEqual(dt.day, 1)
        self.assertEqual(dt.hour, 0)
        self.assertEqual(dt.minute, 0)
        self.assertEqual(dt.second, 0)
        self.assertEqual(dt.microsecond, 0)
        self.assertEqual(dt.utcoffset(), tz.utcoffset(dt))

        dt = datetimes.from_unixtime('1609459199.999999')
        self.assertEqual(dt.year, 2020)
        self.assertEqual(dt.month, 12)
        self.assertEqual(dt.day, 31)
        self.assertEqual(dt.hour, 23)
        self.assertEqual(dt.minute, 59)
        self.assertEqual(dt.second, 59)
        self.assertEqual(dt.microsecond, 999999)
        self.assertEqual(dt.utcoffset(), tz.utcoffset(dt))

    def test_from_javascript(self):
        tz = timezone.get_current_timezone()
        dt = datetimes.from_javascript(0)
        dt = datetimes.from_javascript('0')
        self.assertEqual(dt.year, 1970)
        self.assertEqual(dt.month, 1)
        self.assertEqual(dt.day, 1)
        self.assertEqual(dt.minute, 0)
        self.assertEqual(dt.second, 0)
        self.assertEqual(dt.microsecond, 0)
        self.assertEqual(dt.utcoffset(), tz.utcoffset(dt))

        dt = datetimes.from_javascript('1609459199999')
        self.assertEqual(dt.year, 2020)
        self.assertEqual(dt.month, 12)
        self.assertEqual(dt.day, 31)
        self.assertEqual(dt.hour, 23)
        self.assertEqual(dt.minute, 59)
        self.assertEqual(dt.second, 59)
        self.assertEqual(dt.microsecond, 999000)
        self.assertEqual(dt.utcoffset(), tz.utcoffset(dt))

    def test_datetime_round(self):
        now = timezone.now()

        dt = datetimes.datetime_round(day=True)
        self.assertEqual(dt.hour, 0)
        self.assertEqual(dt.minute, 0)
        self.assertEqual(dt.second, 0)
        self.assertEqual(dt.microsecond, 0)
        self.assertEqual(dt.utcoffset(), now.utcoffset())

        dt = datetimes.datetime_round(hour=True)
        self.assertEqual(dt.minute, 0)
        self.assertEqual(dt.second, 0)
        self.assertEqual(dt.microsecond, 0)
        self.assertEqual(dt.utcoffset(), now.utcoffset())

        dt = datetimes.datetime_round()
        self.assertEqual(dt.second, 0)
        self.assertEqual(dt.microsecond, 0)
        self.assertEqual(dt.utcoffset(), now.utcoffset())

    def test_datetime_local(self):
        now = timezone.now()
        dt = datetimes.datetime_local()
        self.assertEqual(dt.utcoffset(), now.utcoffset())
        # naive
        dt = datetimes.datetime_local(now.replace(tzinfo=None))
        self.assertIsNone(dt.tzinfo)

    @override_settings(TIME_ZONE='Asia/Vladivostok')
    def test_datetime_server(self):
        now = timezone.now()
        dt = datetimes.datetime_server()
        self.assertNotEqual(dt, now)
        self.assertNotEqual(dt.utcoffset(), now.utcoffset())
        # С передачей времени.
        dt = datetimes.datetime_server(now)
        self.assertEqual(dt, now)
        self.assertNotEqual(dt.utcoffset(), now.utcoffset())

    def test_datetime_naive(self):
        dt = datetimes.datetime_naive()
        self.assertIsNone(dt.tzinfo)

    def test_datetime_aware(self):
        dt = datetimes.datetime_naive()
        self.assertIsNone(dt.tzinfo)
        dt = datetimes.datetime_aware(dt)
        self.assertIsNotNone(dt.tzinfo)

    @override_settings(TIME_ZONE='Asia/Vladivostok')
    def test_datetime_server_naive(self):
        now = timezone.now()
        dt = datetimes.datetime_server_naive(now)
        self.assertIsNone(dt.tzinfo)
        # Теперь простое время станет локальным в Asia/Vladivostok.
        dt = dt.replace(tzinfo=now.tzinfo)
        self.assertGreater(dt, now)
        # Разница с UTC составит 10 или 11 часов, в зависимости от перевода
        # часов на летнее время.
        self.assertIn((dt - now).total_seconds(), (10 * 60 * 60, 11 * 60 * 60))

    @override_settings(TIME_ZONE='Asia/Vladivostok')
    def test_datetime_server_aware(self):
        now = timezone.now()
        # Перевод UTC в Asia/Vladivostok.
        dt = datetimes.datetime_server_aware(now)
        self.assertIsNotNone(dt.tzinfo)
        self.assertEqual(dt, now)
        self.assertNotEqual(dt.utcoffset(), now.utcoffset())
        # Перевод простого времени в Asia/Vladivostok.
        dt = datetimes.datetime_server_aware(now.replace(tzinfo=None))
        self.assertIsNotNone(dt.tzinfo)
        self.assertNotEqual(dt.utcoffset(), now.utcoffset())
        # Разница с UTC составит 10 или 11 часов, в зависимости от перевода
        # часов на летнее время.
        self.assertIn((now - dt).total_seconds(), (10 * 60 * 60, 11 * 60 * 60))

    def test_server_date(self):
        self.assertIsInstance(datetimes.server_date(), datetime.date)

    def test_server_time(self):
        self.assertIsInstance(datetimes.server_time(), datetime.time)

    def test_parse_datetime_to_naive(self):
        now = timezone.now()
        val = datetimes.parse_datetime_to_naive(now.isoformat())
        self.assertEqual(val, now.replace(tzinfo=None))

    def test_parse_datetime_to_aware(self):
        now = timezone.now()
        dt = now.replace(tzinfo=None)
        val = datetimes.parse_datetime_to_aware(dt.isoformat())
        self.assertIsNotNone(val.tzinfo)
        self.assertEqual(val, now)

    def test_parse_datetime_to_server_naive(self):
        now = timezone.now()
        val = datetimes.parse_datetime_to_server_naive(now.isoformat())
        self.assertIsNone(val.tzinfo)

    def test_parse_datetime_to_server_aware(self):
        now = timezone.now()
        dt = now.replace(tzinfo=None)
        val = datetimes.parse_datetime_to_server_aware(dt.isoformat())
        self.assertIsNotNone(val.tzinfo)
        self.assertEqual(val, now)


class VladivostokTestCase(SimpleTestCase):

    def setUp(self):
        timezone.activate('Asia/Vladivostok')

    def tearDown(self):
        timezone.activate(timezone.get_default_timezone())

    def test_from_unixtime(self):
        tz = timezone.get_current_timezone()
        dt = datetimes.from_unixtime('0', tz)
        self.assertEqual(dt.year, 1970)
        self.assertEqual(dt.month, 1)
        self.assertEqual(dt.day, 1)
        self.assertEqual(dt.hour, 0)
        self.assertEqual(dt.minute, 0)
        self.assertEqual(dt.second, 0)
        self.assertEqual(dt.microsecond, 0)
        self.assertEqual(dt.utcoffset(), tz.utcoffset(dt))
