#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.test import SimpleTestCase
from djangokit.utils import deep


class DeepTestCase(SimpleTestCase):

    def test_simple(self):
        data = deep.to_dict(None, 'key-1', 1)

        self.assertIn('key-1', data)
        self.assertEqual(data['key-1'], 1)

        value = deep.from_dict(data, 'key-1')
        self.assertEqual(value, 1)

        value = deep.from_dict(data, 'key-2')
        self.assertIsNone(value)

        value = deep.from_dict(data, 'key-2', 2)
        self.assertEqual(value, 2)

        value = deep.from_dict(data, 'key-3', 3, update=True)
        self.assertEqual(value, 3)
        self.assertEqual(data['key-3'], 3)

        value = deep.from_dict(data, 'key-3', 3, update=True, delete=True)
        self.assertEqual(value, 3)
        self.assertNotIn('key-3', data)

        value = deep.from_dict(data, 'key-3', update=True)
        self.assertIsNone(value)
        self.assertIn('key-3', data)

        value = deep.from_dict(data, 'key-3', update=True, delete=True)
        self.assertIsNone(value)
        self.assertNotIn('key-3', data)

        value = deep.from_dict(data, 'key-4', update=True, delete=True)
        self.assertIsNone(value)
        self.assertNotIn('key-4', data)

        value = deep.from_dict(data, 'key-4', 4, update=True, delete=True)
        self.assertEqual(value, 4)
        self.assertNotIn('key-4', data)

        value = deep.from_dict(data, 'key-4', 4, delete=True)
        self.assertEqual(value, 4)
        self.assertNotIn('key-4', data)

    def test_lists(self):
        data = {}

        deep.to_dict(data, 'field', [1, 2, {'key-1': 1, 'key-2': 2}])
        self.assertEqual(data['field'], [1, 2, {'key-2': 2, 'key-1': 1}])

        deep.to_dict(data, 'field', 0)
        self.assertEqual(data['field'], 0)

        deep.to_dict(data, 'field', None)
        self.assertIsNone(data['field'])

    def test_to_flat_list(self):
        data = {
            'int': 1,
            'float': 0.1,
            'str': 'text',
            'true': True,
            'false': False,
            'list': [True, None, False],
            'dict': {'a': 1, 'b': 2},
            'tuple': (3, 1, 2),
            'skip': 1,
        }

        result = deep.to_flat_list(data)
        self.assertIn('int:1', result)
        self.assertIn('float:0.1', result)
        self.assertIn('str:text', result)
        self.assertIn('true:1', result)
        self.assertIn('false:0', result)
        self.assertIn('list:0:1', result)
        self.assertIn('list:1:None', result)
        self.assertIn('list:2:0', result)
        self.assertIn('dict:a:1', result)
        self.assertIn('dict:b:2', result)
        self.assertIn('tuple:0:3', result)
        self.assertIn('tuple:1:1', result)
        self.assertIn('tuple:2:2', result)
        self.assertIn('skip:1', result)

    def test_to_unique_string(self):
        data = {
            'int': 1,
            'float': 0.1,
            'str': 'text',
            'true': True,
            'false': False,
            'list': [True, None, False],
            'dict': {'a': 1, 'b': 2},
            'tuple': (3, 1, 2),
            'skip': 1,
        }
        result = deep.to_unique_string(data, skip=('skip',))
        self.assertEqual(result, (
            'dict:a:1;dict:b:2;'
            'false:0;'
            'float:0.1;'
            'int:1;'
            'list:0:1;list:1:None;list:2:0;'
            'str:text;'
            'true:1;'
            'tuple:0:3;tuple:1:1;tuple:2:2'
        ))
