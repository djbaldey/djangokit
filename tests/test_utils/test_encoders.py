#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
import json
from decimal import Decimal
from uuid import uuid4

from django.test import SimpleTestCase, override_settings
from django.utils.translation import gettext_lazy as _

from djangokit.utils import encoders


class EncodersTestCase(SimpleTestCase):

    @override_settings(LANGUAGE_CODE='en-us')
    def test_JSONEncoder(self):
        uuid = uuid4()
        ctx = {
            'string': 'Строка',
            'uuid': uuid,
            'error': ValueError('message'),
            'promise': _('String'),
            'gen': (n for n in range(2)),
            'decimal': Decimal('99.99'),
        }
        data = json.loads(json.dumps(ctx, cls=encoders.JSONEncoder))
        self.assertEqual(data['string'], 'Строка')
        self.assertEqual(data['uuid'], str(uuid))
        self.assertEqual(data['error'], 'message')
        self.assertEqual(data['promise'], 'String')
        self.assertEqual(data['gen'], [0, 1])
        self.assertEqual(data['decimal'], 99.99)

    @override_settings(LANGUAGE_CODE='en-us')
    def test_dump_to_json(self):
        uuid = uuid4()
        ctx = {
            'string': 'Строка',
            'uuid': uuid,
            'error': ValueError('message'),
            'promise': _('String'),
            'gen': (n for n in range(2)),
            'decimal': Decimal('99.99'),
        }
        data = encoders.dump_from_json(encoders.dump_to_json(ctx))
        self.assertEqual(data['string'], 'Строка')
        self.assertEqual(data['uuid'], str(uuid))
        self.assertEqual(data['error'], 'message')
        self.assertEqual(data['promise'], 'String')
        self.assertEqual(data['gen'], [0, 1])
        self.assertEqual(data['decimal'], 99.99)
