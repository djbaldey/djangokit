#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
import os
from django.test import SimpleTestCase
from djangokit.utils import files


BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class FilesTestCase(SimpleTestCase):

    def test_remove_file(self):
        """Проверка функции молчаливого удаления файла."""
        filename = os.path.join(BASE_DIR, 'Тестовый файл.txt')
        f = open(filename, 'w')
        f.write('text')
        f.close()
        self.assertTrue(os.path.exists(filename))
        result = files.remove_file(filename)
        self.assertTrue(result)
        self.assertFalse(os.path.exists(filename))

    def test_remove_dirs(self):
        """Проверка функции молчаливого удаления каталога."""
        dirname = os.path.join(BASE_DIR, 'dir', 'subdir')
        try:
            os.makedirs(dirname)
        except OSError:
            pass
        self.assertTrue(os.path.exists(dirname))
        filename = os.path.join(dirname, 'Мой тестовый файл.txt')
        f = open(filename, 'w')
        f.write('text')
        f.close()
        # Не удалит каталог, так как там есть файл
        result = files.remove_dirs(dirname)
        self.assertFalse(result)
        # Сначала удалит файлы в каталоге, затем удалит каталог, включая
        # родительский, так как он останется пуст.
        result = files.remove_dirs(dirname, withfiles=True)
        self.assertTrue(result)
        self.assertFalse(os.path.exists(dirname))
        self.assertFalse(os.path.exists(os.path.dirname(dirname)))

    def test_secure_filename(self):
        """Проверка функции создания безопасного названия файла."""
        L = [
            ('', ''),
            ('Простые слова', 'Prostye_slova'),
            ('მარტივი სიტყვები', 'martivi_sitqvebi'),
            ('简单的话', 'Jian_Dan_De_Hua_'),
        ]
        for src, dst in L:
            result = files.secure_filename(src)
            self.assertEqual(result, dst)
