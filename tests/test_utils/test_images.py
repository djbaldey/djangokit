#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
import os
from django.test import SimpleTestCase
from djangokit.utils import images


BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class ImagesTestCase(SimpleTestCase):

    def test_get_or_create_thumbnail(self):
        with self.assertRaises(IOError):
            images.get_or_create_thumbnail('abra.jpg', 'abra.tumb.jpg')
        val = images.get_or_create_thumbnail(
            'abra.jpg', 'abra.tumb.jpg', raise_exception=False)
        self.assertIsNone(val)
        L = ('django.jpg', 'django.png')
        for name in L:
            filename = os.path.join(BASE_DIR, 'img', name)
            thumbname = filename + '.thumb.jpeg'
            val = images.get_or_create_thumbnail(filename, thumbname,
                                                 raise_exception=True)
            self.assertEqual(val, thumbname)
            os.remove(thumbname)
