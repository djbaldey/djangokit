#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.test import SimpleTestCase
from django.utils import timezone
from djangokit.utils import periods


class PeriodsTestCase(SimpleTestCase):

    def get_hours_range(self, hours=0, from_datetime=None):
        dt = from_datetime if from_datetime else timezone.now()
        delta = timezone.timedelta(hours=hours)
        start = dt.replace(minute=0, second=0, microsecond=0) + delta
        end = start.replace(minute=59, second=59, microsecond=999999)
        return start, end

    def test_gethours(self):
        start, end = self.get_hours_range()
        self.assertEqual(periods.gethours(), (start, end))

    def test_gethours_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_hours_range(from_datetime=dt)
        self.assertEqual(periods.gethours(from_datetime=dt), (start, end))

    def test_hour(self):
        start, end = self.get_hours_range()
        self.assertEqual(periods.hour(), (start, end))

    def test_hour_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_hours_range(from_datetime=dt)
        self.assertEqual(periods.hour(from_datetime=dt), (start, end))

    def test_next_hour(self):
        start, end = self.get_hours_range(1)
        self.assertEqual(periods.next_hour(), (start, end))

    def test_next_hour_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_hours_range(1, from_datetime=dt)
        self.assertEqual(periods.next_hour(from_datetime=dt), (start, end))

    def test_previous_hour(self):
        start, end = self.get_hours_range(-1)
        self.assertEqual(periods.previous_hour(), (start, end))

    def test_previous_hour_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_hours_range(-1, from_datetime=dt)
        self.assertEqual(periods.previous_hour(from_datetime=dt), (start, end))

    def get_days_range(self, days=0, from_datetime=None):
        dt = from_datetime if from_datetime else timezone.now()
        delta = timezone.timedelta(days=days)
        start = dt.replace(hour=0, minute=0, second=0, microsecond=0) + delta
        end = start.replace(hour=23, minute=59, second=59, microsecond=999999)
        return start, end

    def test_getdays(self):
        start, end = self.get_days_range()
        self.assertEqual(periods.getdays(), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.getdays(withtime=False), (start_date, end_date))

    def test_getdays_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_days_range(from_datetime=dt)
        self.assertEqual(periods.getdays(from_datetime=dt), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.getdays(withtime=False, from_datetime=dt),
            (start_date, end_date),
        )

    def test_today(self):
        start, end = self.get_days_range()
        self.assertEqual(periods.today(), (start, end))

    def test_today_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_days_range(from_datetime=dt)
        self.assertEqual(periods.today(from_datetime=dt), (start, end))

    def test_tomorrow(self):
        start, end = self.get_days_range(1)
        self.assertEqual(periods.tomorrow(), (start, end))

    def test_tomorrow_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_days_range(1, from_datetime=dt)
        self.assertEqual(periods.tomorrow(from_datetime=dt), (start, end))

    def test_tomorrow2(self):
        start, end = self.get_days_range(2)
        self.assertEqual(periods.tomorrow2(), (start, end))

    def test_tomorrow2_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_days_range(2, from_datetime=dt)
        self.assertEqual(periods.tomorrow2(from_datetime=dt), (start, end))

    def test_yesterday(self):
        start, end = self.get_days_range(-1)
        self.assertEqual(periods.yesterday(), (start, end))

    def test_yesterday_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_days_range(-1, from_datetime=dt)
        self.assertEqual(periods.yesterday(from_datetime=dt), (start, end))

    def test_yesterday2(self):
        start, end = self.get_days_range(-2)
        self.assertEqual(periods.yesterday2(), (start, end))

    def test_yesterday2_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_days_range(-2, from_datetime=dt)
        self.assertEqual(periods.yesterday2(from_datetime=dt), (start, end))

    def test_next2days(self):
        start, end = periods.getdays(dst=(1, 2))
        self.assertEqual(periods.next2days(), (start, end))

    def test_next2days_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = periods.getdays(dst=(1, 2), from_datetime=dt)
        self.assertEqual(periods.next2days(from_datetime=dt), (start, end))

    def test_next3days(self):
        start, end = periods.getdays(dst=(1, 3))
        self.assertEqual(periods.next3days(), (start, end))

    def test_next3days_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = periods.getdays(dst=(1, 3), from_datetime=dt)
        self.assertEqual(periods.next3days(from_datetime=dt), (start, end))

    def test_last2days(self):
        start, end = periods.getdays(dst=(-2, -1))
        self.assertEqual(periods.last2days(), (start, end))

    def test_last2days_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = periods.getdays(dst=(-2, -1), from_datetime=dt)
        self.assertEqual(periods.last2days(from_datetime=dt), (start, end))

    def test_last3days(self):
        start, end = periods.getdays(dst=(-3, -1))
        self.assertEqual(periods.last3days(), (start, end))

    def test_last3days_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = periods.getdays(dst=(-3, -1), from_datetime=dt)
        self.assertEqual(periods.last3days(from_datetime=dt), (start, end))

    def get_week_range(self, weeks=0, firstweekday=1, from_datetime=None):
        dt = from_datetime if from_datetime else timezone.now()
        start = dt - timezone.timedelta(days=dt.weekday() - firstweekday + 1)
        start = start.replace(hour=0, minute=0, second=0, microsecond=0)
        start += timezone.timedelta(days=weeks * 7)
        end = start.replace(hour=23, minute=59, second=59, microsecond=999999)
        end += timezone.timedelta(days=6)
        return start, end

    def test_week(self):
        start, end = self.get_week_range()
        self.assertEqual(periods.week(), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.week(withtime=False), (start_date, end_date))
        start, end = self.get_week_range(firstweekday=0)
        self.assertEqual(periods.week(firstweekday=0), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.week(withtime=False, firstweekday=0),
            (start_date, end_date))

    def test_week_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_week_range(from_datetime=dt)
        self.assertEqual(periods.week(from_datetime=dt), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.week(withtime=False, from_datetime=dt),
            (start_date, end_date),
        )
        start, end = self.get_week_range(firstweekday=0, from_datetime=dt)
        self.assertEqual(periods.week(firstweekday=0, from_datetime=dt),
                         (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.week(withtime=False, firstweekday=0, from_datetime=dt),
            (start_date, end_date))

    def test_next_week(self):
        start, end = self.get_week_range(1)
        self.assertEqual(periods.next_week(), (start, end))
        start, end = self.get_week_range(1, firstweekday=0)
        self.assertEqual(periods.next_week(firstweekday=0), (start, end))

    def test_next_week_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_week_range(1, from_datetime=dt)
        self.assertEqual(periods.next_week(from_datetime=dt), (start, end))
        start, end = self.get_week_range(1, firstweekday=0, from_datetime=dt)
        self.assertEqual(periods.next_week(firstweekday=0, from_datetime=dt),
                         (start, end))

    def test_previous_week(self):
        start, end = self.get_week_range(-1)
        self.assertEqual(periods.previous_week(), (start, end))
        start, end = self.get_week_range(-1, firstweekday=0)
        self.assertEqual(periods.previous_week(firstweekday=0), (start, end))

    def test_previous_week_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_week_range(-1, from_datetime=dt)
        self.assertEqual(periods.previous_week(from_datetime=dt), (start, end))
        start, end = self.get_week_range(-1, firstweekday=0, from_datetime=dt)
        self.assertEqual(
            periods.previous_week(firstweekday=0, from_datetime=dt),
            (start, end))

    def test_get_next_month(self):
        self.assertEqual(
            periods.get_next_month(timezone.datetime(2019, 1, 1)),
            timezone.datetime(2019, 2, 1))
        self.assertEqual(
            periods.get_next_month(timezone.datetime(2019, 1, 15)),
            timezone.datetime(2019, 2, 1))
        self.assertEqual(
            periods.get_next_month(timezone.datetime(2019, 1, 31)),
            timezone.datetime(2019, 2, 1))
        self.assertEqual(
            periods.get_next_month(timezone.datetime(2019, 12, 1)),
            timezone.datetime(2020, 1, 1))
        self.assertEqual(
            periods.get_next_month(timezone.datetime(2019, 12, 15)),
            timezone.datetime(2020, 1, 1))
        self.assertEqual(
            periods.get_next_month(timezone.datetime(2019, 12, 31)),
            timezone.datetime(2020, 1, 1))

    def get_month_range(self, dst=0, from_datetime=None):
        dt = from_datetime if from_datetime else timezone.now()
        fd = dt.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        month = fd.month
        year = fd.year
        if dst < 0:
            year = year - ((abs(dst) + 12 - month) // 12)
            month = ((month - ((abs(dst)) % 12)) % 12) or 12
        elif dst > 0:
            year = year + ((abs(dst) + month - 1) // 12)
            month = ((month + ((abs(dst)) % 12)) % 12) or 12
        start = fd.replace(year=year, month=month)
        end = periods.get_next_month(start) - timezone.timedelta(microseconds=1)
        return start, end

    def test_month(self):
        start, end = self.get_month_range()
        self.assertEqual(periods.month(), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.month(withtime=False), (start_date, end_date))

    def test_month_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_month_range(from_datetime=dt)
        self.assertEqual(periods.month(from_datetime=dt), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.month(withtime=False, from_datetime=dt),
            (start_date, end_date))

    def test_next_month(self):
        start, end = self.get_month_range(1)
        self.assertEqual(periods.next_month(), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.next_month(withtime=False), (start_date, end_date))

    def test_next_month_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_month_range(1, from_datetime=dt)
        self.assertEqual(periods.next_month(from_datetime=dt), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.next_month(withtime=False, from_datetime=dt),
            (start_date, end_date))

    def test_previous_month(self):
        start, end = self.get_month_range(-1)
        self.assertEqual(periods.previous_month(), (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.previous_month(withtime=False), (start_date, end_date))

    def test_previous_month_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start, end = self.get_month_range(-1, from_datetime=dt)
        self.assertEqual(periods.previous_month(from_datetime=dt),
                         (start, end))
        start_date, end_date = start.date(), end.date()
        self.assertEqual(
            periods.previous_month(withtime=False, from_datetime=dt),
            (start_date, end_date))

    def test_quarter1(self):
        dt = timezone.now()
        start = dt.replace(
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=3, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.quarter1(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.quarter1(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_quarter1_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start = dt.replace(
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=3, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.quarter1(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.quarter1(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_quarter2(self):
        dt = timezone.now()
        start = dt.replace(
            month=4, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=6, day=30, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.quarter2()
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.quarter2(withtime=False)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_quarter2_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start = dt.replace(
            month=4, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=6, day=30, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.quarter2(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.quarter2(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_quarter3(self):
        dt = timezone.now()
        start = dt.replace(
            month=7, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=9, day=30, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.quarter3()
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.quarter3(withtime=False)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_quarter3_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start = dt.replace(
            month=7, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=9, day=30, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.quarter3(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.quarter3(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_quarter4(self):
        dt = timezone.now()
        start = dt.replace(
            month=10, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.quarter4()
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.quarter4(withtime=False)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_quarter4_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start = dt.replace(
            month=10, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.quarter4(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.quarter4(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_halfyear1(self):
        dt = timezone.now()
        start = dt.replace(
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=6, day=30, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.halfyear1()
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.halfyear1(withtime=False)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_halfyear1_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start = dt.replace(
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=6, day=30, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.halfyear1(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.halfyear1(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_halfyear2(self):
        dt = timezone.now()
        start = dt.replace(
            month=7, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.halfyear2()
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.halfyear2(withtime=False)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_halfyear2_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start = dt.replace(
            month=7, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.halfyear2(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.halfyear2(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_year(self):
        dt = timezone.now()
        start = dt.replace(
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.year()
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.year(withtime=False)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_year_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start = dt.replace(
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.year(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.year(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_next_year(self):
        dt = timezone.now()
        start = dt.replace(
            year=dt.year + 1,
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            year=dt.year + 1,
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.next_year()
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.next_year(withtime=False)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_next_year_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start = dt.replace(
            year=dt.year + 1,
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            year=dt.year + 1,
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.next_year(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.next_year(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_previous_year(self):
        dt = timezone.now()
        start = dt.replace(
            year=dt.year - 1,
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            year=dt.year - 1,
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.previous_year()
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.previous_year(withtime=False)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())

    def test_previous_year_from_datetime(self):
        dt = timezone.now() - timezone.timedelta(days=500)
        start = dt.replace(
            year=dt.year - 1,
            month=1, day=1, hour=0, minute=0, second=0, microsecond=0,
        )
        end = dt.replace(
            year=dt.year - 1,
            month=12, day=31, hour=23, minute=59, second=59, microsecond=999999,
        )
        period = periods.previous_year(from_datetime=dt)
        self.assertEqual(period[0], start)
        self.assertEqual(period[1], end)
        period = periods.previous_year(withtime=False, from_datetime=dt)
        self.assertEqual(period[0], start.date())
        self.assertEqual(period[1], end.date())
