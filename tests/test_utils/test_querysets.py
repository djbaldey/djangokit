#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from unittest import skip
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.http.request import HttpRequest
from django.test import TestCase
from djangokit.utils import querysets


class QuerysetsTestCase(TestCase):

    def test_quick_pagination(self):
        User = get_user_model()
        queryset = User.objects.all()
        queryset.bulk_create([
            User(username='u%d' % i) for i in range(11)
        ])
        result, page, limit, has_next = querysets.quick_pagination(
            queryset=queryset, page=2, limit=3, max_limit=1000)
        self.assertEqual(len(result), 3)
        self.assertEqual(page, 2)
        self.assertEqual(limit, 3)
        self.assertTrue(has_next)

    @skip
    def test_dictfetchall(self):
        raise NotImplementedError()

    @skip
    def test_get_object_or_none(self):
        raise NotImplementedError()


class LazyFiltersTestCase(TestCase):

    def test_to_string(self):
        filters = querysets.LazyFilters(
            Q(a=1, b=2),
            user=querysets.CURRENT_USER,
        )
        filters.exclude(
            Q(c=3) | Q(d=4),
            d__in=[1, 2, 3],
        )
        self.assertEqual(str(filters), (
            "(AND: (AND: ('a', 1), ('b', 2)), user=CURRENT_USER) & "
            "~(AND: (OR: ('c', 3), ('d', 4)), d__in=[1, 2, 3])"
        ))

    def test_copy(self):
        src = querysets.LazyFilters(Q(a=1, b=2))
        dst = src.copy().filter(c=3).exclude(d=4)
        self.assertEqual(str(src), "(AND: (AND: ('a', 1), ('b', 2)))")
        self.assertEqual(str(dst), (
            "(AND: (AND: ('a', 1), ('b', 2)), c=3) & ~(AND: d=4)"
        ))

    def test_blank(self):
        filters = querysets.LazyFilters()
        manager = get_user_model().objects
        user = manager.create_user(username='user_1')

        qs = manager.all()
        self.assertEqual(qs.count(), 1)

        request = HttpRequest()
        qs = filters.apply(request, qs)
        self.assertEqual(qs.count(), 1)

        request.user = user
        qs = filters.apply(request, qs)
        self.assertEqual(qs.count(), 1)

    def test_filter_args(self):
        filters = querysets.LazyFilters(Q(is_active=True))
        filters.filter(Q(is_staff=True) | Q(is_superuser=True))
        filters.exclude(Q(id=querysets.CURRENT_USER))
        manager = get_user_model().objects

        manager.create_user(username='user_1')
        manager.create_user(username='user_2', is_staff=True)
        manager.create_user(username='user_3', is_superuser=True)
        user = manager.create_user(username='user_4', is_superuser=True)
        qs = manager.all()
        self.assertEqual(qs.count(), 4)

        request = HttpRequest()
        qs = filters.apply(request, qs)
        self.assertEqual(qs.count(), 3)

        request.user = user
        qs = filters.apply(request, qs)
        self.assertEqual(qs.count(), 2)

    def test_filter_kwargs(self):
        filters = querysets.LazyFilters(is_active=True)
        filters.filter(is_staff=True, is_superuser=True)
        filters.exclude(id=querysets.CURRENT_USER)
        manager = get_user_model().objects

        manager.create_user(username='user_1')
        manager.create_user(username='user_2', is_staff=True)
        manager.create_user(username='user_3', is_staff=True, is_superuser=True)
        user = manager.create_user(username='user_4', is_staff=True, is_superuser=True)
        qs = manager.all()
        self.assertEqual(qs.count(), 4)

        request = HttpRequest()
        qs = filters.apply(request, qs)
        self.assertEqual(qs.count(), 2)

        request.user = user
        qs = filters.apply(request, qs)
        self.assertEqual(qs.count(), 1)
