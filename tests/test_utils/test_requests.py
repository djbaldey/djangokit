#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from unittest.mock import MagicMock

from django.contrib.auth import get_user_model
from django.http.request import HttpRequest
from django.test import SimpleTestCase, TestCase
from djangokit.utils import requests


class RequestsTestCase(SimpleTestCase):

    def test_get_ip_address(self):
        request = HttpRequest()
        func = requests.get_ip_address
        self.assertEqual(func(request), '')
        request.META['REMOTE_ADDR'] = '100.1.1.1'
        self.assertEqual(func(request), '100.1.1.1')
        request.META['HTTP_X_FORWARDED_FOR'] = '100.1.1.2'
        self.assertEqual(func(request), '100.1.1.2')
        request.META['HTTP_X_REAL_IP'] = '100.1.1.3'
        self.assertEqual(func(request), '100.1.1.3')
        self.assertEqual(func(request, ['1']), '')
        self.assertEqual(func(request, ['10']), '')
        self.assertEqual(func(request, ['100.']), '')
        request.META['HTTP_X_REAL_IP'] = '127.0.0.1'
        self.assertEqual(func(request, 'local'), '')
        request.META['HTTP_X_REAL_IP'] = '10.0.0.1'
        self.assertEqual(func(request, 'local'), '')
        request.META['HTTP_X_REAL_IP'] = '192.0.0.1'
        self.assertEqual(func(request, 'local'), '')

    def test_get_cookies_domain2(self):
        request = HttpRequest()

        func = requests.get_cookies_domain2

        request.get_host = MagicMock(return_value='localhost')
        self.assertEqual(func(request), 'localhost')

        request.get_host = MagicMock(return_value='example.com')
        self.assertEqual(func(request), 'example.com')

        request.get_host = MagicMock(return_value='sub.example.com')
        self.assertEqual(func(request), 'example.com')

        request.get_host = MagicMock(return_value='b.a.example.com')
        self.assertEqual(func(request), 'example.com')

        request.get_host = MagicMock(return_value='c.b.a.example.com')
        self.assertEqual(func(request), 'example.com')


class RequestsDatabaseTestCase(TestCase):

    def test_get_user(self):
        manager = get_user_model().objects
        user_1 = manager.create_user(username='user_1')
        user_2 = manager.create_user(username='user_2')

        request = HttpRequest()
        request.user = user_1

        func = requests.get_user

        with self.assertNumQueries(0):
            self.assertEqual(func(request, user_1.pk), user_1)

        with self.assertNumQueries(1):
            self.assertEqual(func(request, user_2.pk), user_2)
