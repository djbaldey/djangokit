#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
import json
from uuid import uuid4

from django.test import SimpleTestCase
from django.utils.encoding import force_str
from django.utils.translation import gettext_lazy as _

from djangokit.utils import responses


class ResponsesTestCase(SimpleTestCase):

    def test_JsonResponse(self):
        """Проверка создания JsonResponse."""
        ctx = {
            'string': 'Строка',
            'uuid': uuid4(),
            'error': ValueError('message'),
            'lazy': _('String'),
        }
        r = responses.JsonResponse(ctx)
        self.assertIsInstance(r, responses.JsonResponse)
        value = force_str(r.getvalue())
        self.assertEqual(value, json.dumps(ctx, cls=responses.JSONEncoder))

    def test_BaseApi(self):
        """Проверка наличия класса BaseApi и метода test у него."""
        instance = responses.BaseApi()
        self.assertTrue(callable(instance.test))
