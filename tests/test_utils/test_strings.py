#
# Copyright (c) 2018, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.test import SimpleTestCase, override_settings
from django.utils.encoding import force_bytes, force_str
from django.utils.translation import activate, gettext_lazy as _

from djangokit.utils import strings


class StringsTestCase(SimpleTestCase):

    def test_cut_text(self):
        """Проверка функции обрезки строк."""
        s = _('1234567890 test string')
        result = strings.cut_text(s, 6)
        self.assertEqual(result, '123456')
        s = force_bytes('Строка байт')
        result = strings.cut_text(s, 6)
        self.assertEqual(result, 'Строка')

    def test_obfuscate_uuid(self):
        """Проверка получения обфусцированной уникальной строки UUID."""
        s = strings.obfuscate_uuid()
        self.assertEqual(len(s), 32)
        self.assertFalse(s.isdigit())
        # self.assertTrue(s.lower() != s or s.upper() != s)

    def test_make_unique_secret(self):
        """Проверка получения длинного уникального ключа."""
        s = strings.make_unique_secret()
        self.assertEqual(len(s), 40)
        self.assertFalse(s.isdigit())
        self.assertNotEqual(s.lower(), s)
        self.assertNotEqual(s.upper(), s)
        # Проверка передачи параметра длины
        s = strings.make_unique_secret(64)
        self.assertEqual(len(s), 64)

    @override_settings(LANGUAGE_CODE='en-us')
    def test_translate_text(self):
        """Проверка перевода текста на определённый язык."""
        text = _('Team not found')
        result = strings.translate_text('ru', text)
        activate('ru')
        text_ru = force_str(text)
        self.assertEqual(result, text_ru)

    @override_settings(LANGUAGE_CODE='en-us')
    def test_translate_text_args(self):
        """Проверка перевода текста с позиционными аргументами."""
        team1 = _('Argentina')
        text = _('Team "%s"')
        # параметры кортежем
        params = (team1,)
        result = strings.translate_text('ru', text, params)
        # параметры списком
        params = [team1]
        result = strings.translate_text('ru', text, params)
        activate('ru')
        text_ru = force_str(text % force_str(team1))
        self.assertEqual(result, text_ru)

    @override_settings(LANGUAGE_CODE='en-us')
    def test_translate_text_kwargs(self):
        """Проверка перевода текста с именованными аргументами."""
        team1 = _('Argentina')
        team2 = _('Jamaica')
        text = _(
            'Team "%(team1)s" vs team "%(team2)s" '
            '(%(team1_score)d:%(team2_score)d)'
        )
        params = {
            'team1': team1, 'team2': team2,
            'team1_score': 5, 'team2_score': 0,
        }
        result = strings.translate_text('ru', text, params)
        activate('ru')
        text_ru = force_str(
            text % {
                'team1': force_str(team1), 'team2': force_str(team2),
                'team1_score': 5, 'team2_score': 0,
            }
        )
        self.assertEqual(result, text_ru)

    def test_translate_text_errors(self):
        """Проверка обработки ошибок для переводов."""
        with self.assertRaises(ValueError):
            strings.translate_text('ru', 'Team "%s" not found', 'Team 1')
        # Автоматически подставляемый язык по-умолчанию.
        strings.translate_text(None, 'Text')
        strings.translate_text('abracadabra', 'Text')
