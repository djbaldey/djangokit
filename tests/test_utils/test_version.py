#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
import os
from django.test import SimpleTestCase
from djangokit import VERSION, __version__
from djangokit.utils import version

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class VersionTestCase(SimpleTestCase):

    def test_version(self):
        self.assertEqual(len(VERSION), 5)
        self.assertIsInstance(VERSION[0], int)
        self.assertIsInstance(VERSION[1], int)
        self.assertIsInstance(VERSION[2], int)
        self.assertIsInstance(VERSION[3], str)
        self.assertIsInstance(VERSION[4], (int, str))
        self.assertTrue(
            __version__.startswith('.'.join([str(x) for x in VERSION[:2]])))

    def test_get_docs_version(self):
        func = version.get_docs_version
        equal = self.assertEqual
        for a in range(10):
            for b in range(10):
                for c in range(10):
                    for d in ('alpha', 'beta', 'rc'):
                        for e in range(10):
                            v = 'dev'
                            equal(func((a, b, c, d, e)), v, (a, b, c, d, e))
                    for d in ('final',):
                        for e in range(10):
                            v = '%s.%s' % (a, b)
                            equal(func((a, b, c, d, e)), v, (a, b, c, d, e))

    def test_get_major_version(self):
        func = version.get_major_version
        equal = self.assertEqual
        for a in range(10):
            for b in range(10):
                for c in range(10):
                    for d in ('alpha', 'beta', 'rc', 'final'):
                        for e in range(10):
                            if c:
                                v = '%s.%s.%s' % (a, b, c)
                            else:
                                v = '%s.%s' % (a, b)
                            equal(func((a, b, c, d, e)), v, (a, b, c, d, e))

    def test_get_complete_version(self):
        v = version.get_complete_version(None)
        self.assertEqual(v, (0, 0, 1, 'alpha', 0))
        with self.assertRaises(AssertionError):
            version.get_complete_version((0, 0, 1, 'alpha'))
        with self.assertRaises(AssertionError):
            version.get_complete_version((0, 0, 1, 'a', 0))
        v = version.get_complete_version(VERSION)
        self.assertEqual(v, VERSION)

    def test_get_version(self):
        v = version.get_version((1, 1, 1, 'alpha', 0))
        self.assertTrue(v.startswith('1.1.1.dev'))
        v = version.get_version((1, 1, 1, 'alpha', 1))
        self.assertEqual(v, '1.1.1a1')
        v = version.get_version((1, 1, 1, 'alpha', 900))
        self.assertEqual(v, '1.1.1a900')
        v = version.get_version((1, 1, 1, 'beta', 0))
        self.assertEqual(v, '1.1.1b0')
        v = version.get_version((1, 1, 1, 'beta', 1))
        self.assertEqual(v, '1.1.1b1')
        v = version.get_version((1, 1, 1, 'beta', 888))
        self.assertEqual(v, '1.1.1b888')
        v = version.get_version((1, 1, 1, 'rc', 0))
        self.assertEqual(v, '1.1.1c0')
        v = version.get_version((1, 1, 1, 'rc', 1))
        self.assertEqual(v, '1.1.1c1')
        v = version.get_version((1, 1, 1, 'rc', 2))
        self.assertEqual(v, '1.1.1c2')
        v = version.get_version((1, 1, 1, 'final', 0))
        self.assertEqual(v, '1.1.1')
        v = version.get_version((1, 1, 1, 'final', 1))
        self.assertEqual(v, '1.1.1')
        v = version.get_version((1, 1, 0, 'final', 0))
        self.assertEqual(v, '1.1')
        v = version.get_version((1, 1, 0, 'final', 9999999))
        self.assertEqual(v, '1.1')

    def test_get_version_semver(self):
        v = version.get_version((1, 1, 1, 'alpha', 0), as_semver=True)
        self.assertTrue(v.startswith('1.1.1-alpha.'))
        v = version.get_version((1, 1, 1, 'alpha', 1), as_semver=True)
        self.assertEqual(v, '1.1.1-alpha.1')
        v = version.get_version((1, 1, 1, 'alpha', 900), as_semver=True)
        self.assertEqual(v, '1.1.1-alpha.900')
        v = version.get_version((1, 1, 1, 'beta', 0), as_semver=True)
        self.assertEqual(v, '1.1.1-beta.0')
        v = version.get_version((1, 1, 1, 'beta', 1), as_semver=True)
        self.assertEqual(v, '1.1.1-beta.1')
        v = version.get_version((1, 1, 1, 'beta', 888), as_semver=True)
        self.assertEqual(v, '1.1.1-beta.888')
        v = version.get_version((1, 1, 1, 'rc', 0), as_semver=True)
        self.assertEqual(v, '1.1.1-rc.0')
        v = version.get_version((1, 1, 1, 'rc', 1), as_semver=True)
        self.assertEqual(v, '1.1.1-rc.1')
        v = version.get_version((1, 1, 1, 'rc', 2), as_semver=True)
        self.assertEqual(v, '1.1.1-rc.2')
        v = version.get_version((1, 1, 1, 'final', 0), as_semver=True)
        self.assertEqual(v, '1.1.1')
        v = version.get_version((1, 1, 1, 'final', 1), as_semver=True)
        self.assertEqual(v, '1.1.1')
        v = version.get_version((1, 1, 0, 'final', 0), as_semver=True)
        self.assertEqual(v, '1.1.0')
        v = version.get_version((1, 1, 0, 'final', 99999999), as_semver=True)
        self.assertEqual(v, '1.1.0')

    def test_get_vcs_changeset(self):
        v = version.get_vcs_changeset(BASE_DIR)
        self.assertTrue(bool(v and v.isnumeric()))
        self.assertIsNone(version.get_vcs_changeset('/tmp/'))
