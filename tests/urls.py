from django.urls import path

from .views import OkView, VisitedView


urlpatterns = [
    path('ok', OkView.as_view()),
    path('visited', VisitedView.as_view()),
    # path('app1/', include('tests.app1.urls')),
    # path('app2/', include('tests.app2.urls')),
]
