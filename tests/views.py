from django.http import HttpResponse
from django.views import View


class OkView(View):

    def get(self, request):
        return HttpResponse('OK', content_type='text/plain; charset=utf-8')


class VisitedView(View):

    def get(self, request):
        request.session['visited'] = 'true'
        return HttpResponse('OK', content_type='text/plain; charset=utf-8')
